const filter = require('../filter') ; 

const items = [1,2,3,4,5,5];

// standard filter implementation
// const res = items.filter(num =>{
//     return num >= 4 ; 
// });
// console.log(res) ; 

const result = filter(items,function(num){
    if( num >= 4){
        return num ; 
    }   
});

console.log(result) ; 