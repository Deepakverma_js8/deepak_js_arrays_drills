const find = require('../find') ;

const items = [1, 2, 3, 4, 5, 5];

// calling builtin find function
// const found = items.find(element => element > 4);
// console.log(found);

const res = find(items,function(num){
    if( num === 4){
        return num ; 
    }
});

console.log(res) ; 