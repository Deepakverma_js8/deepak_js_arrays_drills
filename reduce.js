module.exports = function(items,cb,startingvalue){
    let index = 0 ;
    if(startingvalue === undefined){
        startingvalue = items[0] ;
        index = 1 ; 
    }
    for( i = index ; index < items.length ; index++){
        startingvalue = cb(startingvalue,items[index],index,items) ;
    }
    return startingvalue ; 
}

