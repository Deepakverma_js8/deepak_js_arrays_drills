const result =  function(items,cb){
    const res = [] ; 
    let n = items.length;
    for(let i = 0 ; i < n ; i++){
        if(cb(items[i],i,items)){
             res.push(items[i]) ; 
        }
    }
    return res ; 
}
module.exports = result ; 