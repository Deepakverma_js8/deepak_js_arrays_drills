module.exports = function(arr, callback){
    const res = [];
    for(let index=0; index < arr.length; index++){
        res.push(callback(arr[index], index));
    }
    return res;
}

